Sample web application built with Ruby on Rails to test 5 minute production app.

Features:

- Product file upload: Active Storage local/S3
- User avatar: Active Storage local/S3
- User login: devise
- Database: PostgreSQL

Requirements:
- Define `AWS_ACCESS_KEY_ID`, `AWS_DEFAULT_REGION`, `AWS_SECRET_ACCESS_KEY` in CI/CD variables in project settings.
- Define `GL_VAR_RAILS_MASTER_KEY` with master key to decrypt `config/credentials.yml.enc`
- Everything else you can find in `.gitlab-ci.yml`
