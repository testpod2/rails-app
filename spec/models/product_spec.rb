require 'rails_helper'

RSpec.describe Product, type: :model do
  it "is valid with valid attributes" do
    product = Product.new(title: 'TV', price: 99)

    expect(product).to be_valid
  end

  it "is not valid without a title" do
    product = Product.new(title: nil, price: 99)

    expect(product).to_not be_valid
  end
end
