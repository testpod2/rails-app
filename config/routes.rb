Rails.application.routes.draw do
  devise_for :users
  resources :products
  resource :user_avatar, only: [:new, :create]

  root 'products#index'
end
